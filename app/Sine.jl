module Sine
export sine

sine(x,A,k,ϕ) = A*sin(k*x + ϕ)

end