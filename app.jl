module App
# set up Genie development environment
using GenieFramework
@genietools

include("app/Sine.jl")
using .Sine

import PlotlyBase, PlotlyJS, Plots
Plots.plotlyjs()

# Define the range of x values
xs = range(-8π,8π,length=1000)

function plotly_traces(plt::Plots.Plot)
    traces = PlotlyBase.GenericTrace[]
    for series_dict in Plots.plotly_series(plt)
        plotly_type = pop!(series_dict, :type)
        push!(traces, PlotlyBase.GenericTrace(plotly_type; series_dict...))
    end
    return traces
end

# Define a function to update the plot
function update_plot(A,k,ϕ)
    result = Sine.sine.(xs,A,k,ϕ)
    plot(xs,result,show=false)

    # Return the plot for display
    return PlotlyJS.plotly(p)
end

#=
layout = Layout(
    title="Sine function",
    xaxis=attr(
        title="x",
        showgrid=false
    ),
    yaxis=attr(
        title="y",
        showgrid=true,
        range=[0, 20]
    )
)
=#

# Define default values for reactive variables
A_default = 0.0
k_default = 0.0
ϕ_default = 0.0

# add reactive code to make the UI interactive
@app begin
    # reactive variables are tagged with @in and @out
    @in A = 0.0
    @in k = 0.0
    @in ϕ = 0.0
    @out traces = update_plot(A,k,ϕ)
    
    @onchange A, k, ϕ begin
        traces = update_plot(A,k,ϕ)
    end
end


# register a new route and the page that will be
# loaded on access
@page("/", "app.jl.html")
end
